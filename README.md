# Desafio Conductor de Seleção

Olá, queremos convidá-lo a participar de nosso desafio de seleção. Pronto para participar? Seu trabalho será visto por nosso time e você receberá ao final um feedback sobre o que achamos do seu trabalho. Não é legal?

## Sobre a oportunidade

A vaga é para Desenvolvedor Mobile (Android), temos vagas com diversos níveis de senioridade e para cada um deles utilizaremos critérios específicos considerando esse aspecto, combinado?
Se você for aprovado nesta etapa, será convidado para uma entrevista final com nosso time técnico.

## Desafio Técnico

Desenvolver uma aplicação seguindo uma arquitetura, padrões e ferramentas de mercado.

Será um aplicativo de visualização de extrato de cartão, filtrado por mês com paginação, e um gráfico das compras mensais (nome da loja no eixo X, barra com o valor da compra no eixo Y).

O projeto deverá gerar dois Flavors para Android, o BlueCard e o GreenCard, onde mudará a cor de fundo do cartão e a cor de fundo da TabBar.

- Pré-requisitos:
  - Desenvolver utilizando linguagem nativa;
- Opcionais:
  - RxJava;
  - Injeção de dependências;

- O que esperamos como escopo mínimo:
  - Visualizar Extrato do cartão;
  - Filtragem paginada do extrato;
  - Gráfico de compras;
  - Alteração da cor de fundo, seguindo a regra do BlueCard e GreenCard;
- O que vamos avaliar:
  - Seu código;
  - Organização;
  - Boas práticas;
  - Garantia de qualidade do desenvolvimento (Testes unitários e de instrumentação);

### Exemplos de telas

- Android:
  - [Adobe XD Spec](https://xd.adobe.com/spec/dd17fb1e-6a3a-47a7-5195-48494891a426-ab26/)
  - [Adobe XD Prototype](https://xd.adobe.com/view/0a63fe08-096c-4057-5ec9-90d6240cbd07-7c52/)

### Instruções

 1. Faça o fork do desafio;
 2. Desenvolva. Você terá 3 (três) dias a partir da data do envio do desafio;
 3. Crie um arquivo de texto com a nomenclatura README.MD com a explicação de como devemos executar o projeto e com uma descrição do que foi feito;
 4. Após concluir seu trabalho envie o que foi desenvolvido em um arquivo .zip para o e-mail daniel.silva@conductor.com.br

Desejamos boa sorte e que a força esteja com você!
